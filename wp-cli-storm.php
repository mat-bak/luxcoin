<?php
$commend = $argv[1];
$config = json_decode(file_get_contents('wp-cli-storm.json'));

function prepareOutput($array) {
    return implode("\n", $array);
}

switch($commend) {
    case 'show:config':
        print_r($config);

        break;
    case 'install':
        echo 'Pobieranie WP' . "\n";
        passthru('php wp-cli.phar core download', $output);
        echo 'Gotowe'."\n\n";

        echo 'Tworzenie wp-config.php' . "\n";

        if(!file_exists('wp-config.php')) {
            passthru('cp wp-config-sample.php wp-config.php');
            echo 'Gotowe'."\n\n";
        } else {
            echo 'Gotowe, wp-config.php juz istnial'."\n\n";
        }

        echo 'Uwtorz baze mysql o nazwie projektu - "' . $config->domain . '" i skonfiguruj jej dane w wp-config.php. wpisz "y" by kontynuowac (n)' . "\n";

        $confirmation  =  trim( fgets( STDIN ) );
        if ( $confirmation !== 'y' ) {
            echo 'Skonfiguruj dane bazy danych i uruchom instalację ponownie'."\n";
            exit(0);
        }

        // Instalacja WP
        echo 'Instalacja WP' . "\n";
        passthru('php wp-cli.phar core install --url=http://' . $config->domain . '/ --title=Title_page --admin_user=admin --admin_password=admin --admin_email=admin@example.com');
        echo 'Gotowe'."\n\n";

        // Instalacja pluginow
        echo 'Instalacja pluginow' . "\n";
        foreach($config->plugins as $plugin) {
            passthru('php wp-cli.phar plugin install ' . $plugin->name . (!$plugin->active ?: ' --activate'));
        }
        echo 'Gotowe'."\n\n";

        echo 'Instalacja zakonczona powodzeniem'."\n";
        break;
}