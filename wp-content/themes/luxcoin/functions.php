<?php
/**
 * Under Timber functions and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Under_Timber
 */

if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
    add_action('admin_notices', function() {
        echo '<div class="error"><p>Timber not installed. Make sure you run a composer install in the theme directory</p></div>';
    });
    return;
}
else {
    require_once(__DIR__ . '/vendor/autoload.php');
    $timber = new \Timber\Timber();
}

class ClickCommunitySite extends TimberSite {

    function __construct(){

        add_action( 'wp_enqueue_scripts', [$this, 'theme_styles']);
        add_action( 'admin_enqueue_scripts',[$this, 'load_custom_wp_admin_style'] );
        add_action( 'wp_enqueue_scripts', [$this, 'theme_scripts'], 10, 1);
        add_action('after_setup_theme', [$this, 'register_menus']);
        add_action('after_setup_theme', [$this, 'theme_acf_options']);
        add_action('after_setup_theme', [$this, 'add_sidebars']);
        add_action( 'init', [$this, 'register_post_types'] );
	    add_action('acf/init', [$this, 'theme_acf_options']);
	    add_action('after_setup_theme', function () {
		    add_theme_support('woocommerce');
	    });

        add_filter('timber_context', [$this, 'add_to_context']);

        parent::__construct();
    }
    /**
     * Add values to the universal Timber context.
     *
     * @param array $context
     *
     * @return array $context
     */
    function add_to_context( $context ) {
        // Our menu occurs on every page, so we add it to the global context.
        $context['menu'] = array(
            'header_primary' => new TimberMenu('header_menu'),
            'footer_secondary' => new TimberMenu('footer_menu')
        );

//        $context['menu_sidebar'] = new TimberMenu( 'sidebar' );
        $context['home_url']  = get_home_url( NULL, '/' );
        $context['site']      = $this;

        // ACF Support Options page
        if(function_exists('get_fields')){
            $context['options']   = get_fields('options');
        }

        $context['post_type'] = get_post_type();

        // breadCrump
        if(function_exists('yoast_breadcrumb')){
            $context['breadcrumb'] = yoast_breadcrumb('<p>','</p>', false );
        }

//	    $context['widget_hero_home'] = Timber::get_widgets( 'hero-homepage' );

	    $context['languages'] = icl_get_languages( 'orderby=KEY&order=DIR&link_empty_to=/' );

        return $context;
    }

    function theme_styles() {
        wp_enqueue_style( '_t-style', get_stylesheet_uri() );
        wp_enqueue_style( '_s-style', get_template_directory_uri() . '/build/css/main.css' );
    }

    function load_custom_wp_admin_style() {
//        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/build/css/styles-admin.css', false, '1.0.0' );
//        wp_enqueue_style( 'custom_wp_admin_css' );

//        wp_register_script( 'custom_wp_admin_js', get_template_directory_uri() . '/build/js/admin.js', false, '1.0.0' );
//        wp_enqueue_script( 'custom_wp_admin_js' );

        if (is_admin()) {
//            add_editor_style( get_template_directory_uri() . '/build/css/editor-style.css');
        }
    }

    function theme_scripts() {
        wp_enqueue_script( 'main-scripts', get_template_directory_uri() . '/build/js/scripts.js', array('jquery','wp-util'), FALSE, TRUE );
    }

	function theme_acf_options() {

		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title' 	=> 'Options theme',
				'menu_title'	=> 'Options theme',
				'menu_slug' 	=> 'theme-settings',
				'capability'	=> 'edit_posts',
				'redirect'		=> false
			));
		}
	}

    function add_sidebars() {

    }

    function register_menus() {
        register_nav_menus( array(
            'header_menu' => esc_html__( 'Header menu', '_t' ),
            'footer_menu' => esc_html__( 'Footer menu', '_t' )
        ) );
    }

	function register_widgets () {
//		register_sidebar( array(
//			'name'          => 'Hero homepage',
//			'id'            => 'hero-homepage',
//			'before_widget' => '',
//			'after_widget'  => '',
//			'before_title'  => '',
//			'after_title'   => '',
//		) );
	}

    function theme_support() {

    }

    function register_post_types() {
//	    $args_testimonial = array(
//		    'labels'             => array(
//			    'name'               => __( 'Testimonials', 'post type general name', 'luxcoin' ),
//			    'singular_name'      => __( 'Testimonial', 'post type singular name', 'luxcoin' ),
//			    'menu_name'          => __( 'Testimonials', 'admin menu', 'luxcoin' ),
//			    'name_admin_bar'     => __( 'Testimonials', 'add new on admin bar', 'luxcoin' ),
//			    'add_new'            => __( 'Add Testimonial', 'book', 'luxcoin' ),
//			    'add_new_item'       => __( 'Add New Testimonial', 'luxcoin' ),
//			    'new_item'           => __( 'New Testimonial', 'luxcoin' ),
//			    'edit_item'          => __( 'Edit Testimonial', 'luxcoin' ),
//			    'view_item'          => __( 'View Testimonial', 'luxcoin' ),
//			    'all_items'          => __( 'All Testimonials', 'luxcoin' ),
//			    'search_items'       => __( 'Search Testimonials', 'luxcoin' ),
//			    'parent_item_colon'  => __( 'Parent Testimonials:', 'luxcoin' ),
//			    'not_found'          => __( 'No Testimonials found.', 'luxcoin' ),
//			    'not_found_in_trash' => __( 'No Testimonials found in Trash.', 'luxcoin' )
//		    ),
//		    'description'        => __( 'Description.', 'luxcoin' ),
//		    'public'             => true,
//		    'publicly_queryable' => true,
//		    'show_ui'            => true,
//		    'show_in_menu'       => true,
//		    'query_var'          => true,
//		    'rewrite'            => array( 'slug' => 'testimonial' ),
//		    'capability_type'    => 'post',
//		    'has_archive'        => true,
//		    'hierarchical'       => false,
//		    'menu_position'      => 50,
//		    'supports'           => array( 'title', 'editor', 'thumbnail' )
//	    );
//
//	    register_post_type( 'testimonial', $args_testimonial );
    }

	function vc_remove_shortcodes () {
		// Remove VC Elements
		if( function_exists('vc_remove_element') ){

			// Remove VC Button Element
			vc_remove_element( 'vc_btn' );

			// Remove VC Separator Element
			//vc_remove_element( 'vc_separator' );

		}
	}
}
new ClickCommunitySite();


if ( ! function_exists( '_t_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function _t_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Under Timber, use a find and replace
         * to change '_t' to the name of your theme in all the template files.
         */
        load_theme_textdomain( '_t', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );


        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

//        add_theme_support( 'custom-logo', array(
//            'height'      => 100,
//            'width'       => 400,
//            'flex-height' => true,
//            'flex-width'  => true,
//            'header-text' => array( 'site-title', 'site-description' ),
//        ) );

//        add_post_type_support( 'page', 'excerpt' );
    }
endif;
add_action( 'after_setup_theme', '_t_setup' );

/**
 * ACF Default fields
 */
if ( function_exists('acf_add_local_field_group') ) {
	require get_template_directory() . '/lib/customizer-acf.php';
}

