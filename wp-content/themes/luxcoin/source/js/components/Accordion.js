import $ from "jquery";

class Accordion {
    constructor($container) {
        this.$triggers = $container.find('.js-accordion__trigger');
    }

    events() {
        this.$triggers.on('click', (e) => {
            e.preventDefault();
            let $target = $(e.target);
            let $parent =  $target.parent();
            let $child = $parent.find('.js-accordion__child').first();
            $parent.toggleClass('open');
            $child.slideToggle();
        });
    }

    init() {
        this.events();
    }
}

export default Accordion;