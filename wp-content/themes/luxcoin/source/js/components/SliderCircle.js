import $ from "jquery";

class SliderCircle {
    constructor( $container ) {
        this.$container = $container;
        this.$triggers = this.$container.find('.glass');
    }

    events() {
        this.$triggers.on('click', (e) => {
            e.preventDefault();
            this._updatePosition( $(e.currentTarget).attr('data-item') );
        });
    }

    _updatePosition(item) {
        this.$container.removeClass( this.$container.attr('data-position') );
        this.$container.attr('data-position', 'slider-circle--'+item);
        this.$container.addClass('slider-circle--'+item);
    }

    init() {
        this.events();
    }
}

export default SliderCircle;