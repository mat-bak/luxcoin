import $ from "jquery";

class MainNav {
    constructor($nav_container) {
        this.$window = $(window);
        this.$header = $('.page-header');
        this.$container = $nav_container;
        this.$trigger = this.$container.find('.js-main-nav-trigger');
        this.navigation = this.$container.find('.js-main-nav');

        this.$submenuItems = this.navigation.find('.js-main-nav-drop');
    }

    events() {
        console.log(this.$header);
        this.$window.on('scroll', () => {
            if (window.pageYOffset > 0) {
                this.$header.addClass("page-header--sticky");
            } else {
                this.$header.removeClass("page-header--sticky");
            }
        });
        this.$trigger.on('click', (e) => {
            e.preventDefault();
            this.navigation.toggleClass('main-nav--open');
            this.$trigger.toggleClass('main-nav-trigger--open');
        });

        this.$submenuItems.each((i, item) => {
            const $item = $(item);
            $item.find('.js-main-nav-drop__trigger').on('click', (e) => {
                e.preventDefault();
                $item.toggleClass('main-nav-drop--open');
            });
        });
    }

    init() {
        this.events();
    }
}

export default MainNav;