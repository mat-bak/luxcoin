import $ from "jquery";

class CoinsSlider {
    constructor( $container ) {
        this.$container = $container;
        this.$arrowPrev = this.$container.find('.js-slider-coins__prev');
        this.$arrowNext = this.$container.find('.js-slider-coins__next');

        this.next = true;

        this.coins = [
            {
                $item: this.$container.find('.left'),
                value: 'left',
                content: this.$container.find('.left').find('.js-slider-coins__content')
            },
            {
                $item: this.$container.find('.center'),
                value: 'center',
                content: this.$container.find('.center').find('.js-slider-coins__content')
            },
            {
                $item: this.$container.find('.right'),
                value: 'right',
                content: this.$container.find('.right').find('.js-slider-coins__content')
            },
        ];
    }

    events() {
        this.$arrowPrev.on('click', (e) => {
            e.preventDefault();
            this.next = 0;
            this._updatePositions();
        });
        this.$arrowNext.on('click', (e) => {
            e.preventDefault();
            this.next = 1;
            this._updatePositions();
        });
    }

    _updatePositions() {
        this.coins.forEach( (item) => {
            item.$item.removeClass(item.value);
            item.content.fadeOut(300);
            if (item.value === 'center') {
                item.value = (this.next)?'left':'right';
            } else if (item.value === 'left') {
                item.value = (this.next)?'right':'center';
            } else if (item.value === 'right') {
                item.value = (this.next)?'center':'left';
            }
            item.$item.addClass(item.value);
            item.$item.attr('data-place', item.value);

            if (item.value === 'center') {
                item.content.fadeIn(300);
            }
        });
    }

    init() {
        this.events();
    }
}

export default CoinsSlider;