import $ from "jquery";

class LanguageSwitch {
    constructor( $el ) {
        this.$el = $el;
    }

    init() {
        this.$el.find('.lang-select__trigger').on('click', e => {
            e.preventDefault();
            this.$el.toggleClass('is-open');
        });
    }
}

export default LanguageSwitch;