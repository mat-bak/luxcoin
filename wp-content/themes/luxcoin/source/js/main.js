import Header from './components/MainNav';
import Accordion from './components/Accordion';
import LanguageSwitch from './components/LanguageSwitch';
import CoinsSlider from './components/CoinsSlider';
import SliderCircle from './components/SliderCircle';

(function( $ ) {

    'use strict';

    const header = new Header( $('.js-nav-main-container') );
    header.init();

    const langSwitchersArray = [];
    $('.js-lang-select').each( (i, item) => {
        langSwitchersArray[i] = new LanguageSwitch( $(item) );
        langSwitchersArray[i].init();
    });

    const coinsSliderRun = new CoinsSlider( $('.js-slider-coins') );
    coinsSliderRun.init();

    const circleSliderRun = new SliderCircle( $('.js-slider-circle') );
    circleSliderRun.init();

    let loader = {
        $loading: $('.js-loading-page'),
        $body: $('body'),
        init: function () {
            setTimeout(function () {
                loader.hide();
            }, 4000);
        },
        hide: function () {
            setTimeout(function () {
                loader.$loading.fadeOut(300);
                loader.$body.removeClass('js-body-loading');
            }, 1000);
        }
    };

    $( window ).on( "load", function() {
        loader.hide();
    });

    AOS.init({
        disable: 'mobile'
    });

    const accordions = [];
    $('.js-accordion').each( (i, item) => {
        accordions[i] = new Accordion( $(item) );
        accordions[i].init();
    });




    // makes the parallax elements
    function parallaxIt() {

        // create variables
        var $fwindow = $(window);
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

        // on window scroll event
        $fwindow.on('scroll resize', function() {
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        });

        // for each of content parallax element
        $('[data-type="content"]').each(function (index, e) {
            let $contentObj = $(this);
            let fgOffset = parseInt($contentObj.offset().top);
            let yPos;

            $fwindow.on('scroll resize', function (){
                yPos = ((fgOffset - scrollTop) - ($fwindow.height()/2))/5;

                $contentObj.css({
                    "-webkit-transform":"translateY("+yPos+"px)",
                    "-ms-transform":"translateY("+yPos+"px)",
                    "transform":"translateY("+yPos+"px)"
                });
            });
        });

        // triggers winodw scroll for refresh
        $fwindow.trigger('scroll');
    }

    parallaxIt();
})(jQuery);
