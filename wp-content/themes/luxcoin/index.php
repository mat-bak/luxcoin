<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */
$context = Timber::get_context();
$templates = array( 'Templates/index.twig' );

$context['is_front'] = is_front_page();


if ( is_front_page() ) {
	$context['page'] = new TimberPost();
	array_unshift( $templates, 'Templates/front-page.twig' );
}
Timber::render( $templates, $context );