<?php 
/* Template Name: Spirit Coin */ 

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}

$context = Timber::get_context();

$context['page'] = new TimberPost();
Timber::render( 'Templates/spirit-coin.twig', $context );


?>